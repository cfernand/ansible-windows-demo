---
aap_host: "{{ aap_host }}"
aap_user: "{{ aap_user }}"
aap_pass: "{{ aap_pass }}"

python_interpreter: "{% raw %}{{ ansible_playbook_python }}{% endraw %}"

organizations: []

teams: []

users: []

admin_users: []

auditor_users: []

inventories: 
  - name: "Automation Controller Inventory"
    desc: "Automation Controller Inventory"
    org: Default
  - name: "Azure Inventory"
    desc: "Automation Controller Inventory"
    org: Default
    vars: { "ansible_connection": "winrm", "ansible_port": "5986", "ansible_winrm_transport": "basic", "ansible_winrm_server_cert_validation": "ignore" }

inventory_sources:
  - name: "Azure Inventory Source"
    desc: "Azure Inventory Source"
    inv: "Azure Inventory"
    cred: "Azure Credential"
    overwrite: true
    update_on_launch: false
    source: azure_rm
    org: Default
    source_vars: { "hostnames": "tags.instance_name" }

hosts: 
  - name: localhost
    desc: "localhost"
    inv: "Automation Controller Inventory"
    vars: { "ansible_connection": "local", "ansible_python_interpreter": "{% raw %}{{ python_interpreter }}{% endraw %}" }

execution_enviroment:
  - name: "Azure Execution Environment"
    desc: "quay.io/cfernand/ee-azure-image:1.3"

scm_custom_credential_types: 
  - name: "Azure Resource Group"
    kind: cloud
    inputs: "{{ lookup('file', 'credential-types/azure-resource-group-inputs.json') }}"
    injectors: "{{ lookup('file', 'credential-types/azure-resource-group-injectors.json') }}"

scm_credentials:
  - name: "Red Hat Ansible Automation Platform Credential"
    desc: "Red Hat Ansible Automation Platform Credential"
    org: Default
    type: "Red Hat Ansible Automation Platform"
    inputs:
      host: "{{ aap_host }}"
      username: "{{ aap_user }}"
      password: "{{ aap_pass }}"
  - name: "Azure Credential"
    desc: "Azure Credential"
    org: Default
    type: "Microsoft Azure Resource Manager"
    inputs:
      subscription: "{{ azure_subscription_id }}"
      client: "{{ azure_client_id }}"
      secret: "{{ azure_client_secret }}"
      tenant: "{{ azure_tenant_id}}"
  - name: "Azure Resource Group"
    desc: "Azure Resource Group"
    org: Default
    type: "Azure Resource Group"
    inputs:
      azure_resource_group: "{{ azure_resource_group }}"
  - name: "Windows Credential"
    desc: "Windows Credential"
    org: Default
    type: "Machine"
    inputs:
      username: "ansible"
      password: "{{ windows_password }}"

scm_projects:
  - name: "Windows Demo Environment Project"
    desc: "Windows Demo Environment Project"
    org: Default
    git_url: "https://gitlab.com/cfernand/ansible-windows-demo.git"
    git_branch: main
    git_cred: ""
    update_on_launch: no

scm_jobtemplates:
  - name: "[JT][WIN DEMO] Create Azure Network"
    org: Default
    inv: "Automation Controller Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/azure-create-network.yml"
    creds: 
      - "Azure Credential"
      - "Azure Resource Group"
    ask_limit_on_launch: no
    ask_variables_on_launch: no
    allow_simultaneous: no
    survey_enabled: yes
    survey_spec: "{{ lookup('file', 'survey-specs/azure-create-network.json') }}"
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Remove Azure Network"
    org: Default
    inv: "Automation Controller Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/azure-remove-network.yml"
    creds:
      - "Azure Credential"
      - "Azure Resource Group"
    ask_limit_on_launch: no
    ask_variables_on_launch: no
    allow_simultaneous: no
    survey_enabled: yes
    survey_spec: "{{ lookup('file', 'survey-specs/azure-remove-network.json') }}"
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Deploy Azure Instance"
    org: Default
    inv: "Automation Controller Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/azure-deploy-instance.yml"
    creds:
      - "Azure Credential"
      - "Azure Resource Group"
    ask_limit_on_launch: no
    ask_variables_on_launch: no
    allow_simultaneous: yes
    survey_enabled: yes
    survey_spec: "{{ lookup('file', 'survey-specs/azure-deploy-instance.json') }}"
    extra_vars: { "azure_os_type": "windows", "azure_image_offer": "WindowsServer", "azure_image_publisher": "MicrosoftWindowsServer", "azure_storage_type": "StandardSSD_LRS", "instance_flavor": "Standard_D8_v3", "private_network": "windows-network", "windows_admin_password": "{{ windows_password }}" }
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Remove Azure Instance"
    org: Default
    inv: "Automation Controller Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/azure-remove-instance.yml"
    creds:
      - "Azure Credential"
      - "Azure Resource Group"
    ask_limit_on_launch: no
    ask_variables_on_launch: no
    allow_simultaneous: yes
    survey_enabled: yes
    survey_spec: "{{ lookup('file', 'survey-specs/azure-remove-instance.json') }}"
    extra_vars: { "remove": "yes" }
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Deploy Windows Myrtille"
    org: Default
    inv: "Azure Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/windows-deploy-myrtille.yml"
    creds: 
      - "Windows Credential"
    ask_limit_on_launch: yes
    ask_variables_on_launch: no
    allow_simultaneous: no
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Deploy Windows Domain Controller"
    org: Default
    inv: "Azure Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/windows-deploy-dc.yml"
    creds: 
      - "Windows Credential"
    ask_limit_on_launch: yes
    ask_variables_on_launch: no
    allow_simultaneous: no
    extra_vars: { "ansible_windows_domain_member": "", "pdc_administrator_username": "Administrator", "pdc_administrator_password": "{{ windows_password }}", "pdc_domain_safe_mode_password": "{{ windows_password }}" }
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Deploy Windows Domain Controller"
    org: Default
    inv: "Azure Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/windows-deploy-dc.yml"
    creds: 
      - "Windows Credential"
    ask_limit_on_launch: yes
    ask_variables_on_launch: no
    allow_simultaneous: no
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Join Windows Client to a Domain"
    org: Default
    inv: "Azure Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/windows-join-dc.yml"
    creds:
      - "Azure Credential"
      - "Windows Credential"
    ask_limit_on_launch: no
    ask_variables_on_launch: no
    allow_simultaneous: no
    extra_vars: { "pdc_administrator_username": "Administrator@redhat.local", "pdc_administrator_password": "{{ windows_password }}", "pdc_dns_nics": "*", "pdc_domain": "redhat.local" }
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Deploy Windows Server Update Services"
    org: Default
    inv: "Azure Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/windows-deploy-wsus.yml"
    creds: 
      - "Windows Credential"
    ask_limit_on_launch: yes
    ask_variables_on_launch: yes
    allow_simultaneous: no
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Configure Windows Firewall"
    org: Default
    inv: "Azure Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/windows-firewall.yml"
    creds:
      - "Windows Credential"
    ask_limit_on_launch: yes
    ask_variables_on_launch: no
    allow_simultaneous: no
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Create WSUS Group Policy"
    org: Default
    inv: "Azure Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/windows-gpo-wsus.yml"
    creds:
      - "Azure Credential"
      - "Windows Credential"
    ask_limit_on_launch: yes
    ask_variables_on_launch: no
    allow_simultaneous: no
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Join Windows Client to a WSUS"
    org: Default
    inv: "Azure Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/windows-join-wsus.yml"
    creds:
      - "Windows Credential"
    ask_limit_on_launch: no
    ask_variables_on_launch: yes
    allow_simultaneous: no
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Deploy Windows Internet Information Services"
    org: Default
    inv: "Azure Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/windows-deploy-iis.yml"
    creds:
      - "Windows Credential"
    ask_limit_on_launch: yes
    ask_variables_on_launch: no
    allow_simultaneous: no
    extra_vars: { "ansible_site_path": "c:\\inetpub\\wwwroot\\ansibletest", "ansible_test_staging_path": "c:\\deploy\\ansible-test-site" }
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Install Windows Updates"
    org: Default
    inv: "Azure Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/windows-update.yml"
    creds:
      - "Windows Credential"
    ask_limit_on_launch: yes
    ask_variables_on_launch: no
    allow_simultaneous: no
    extra_vars: { "win_update_blacklist": "[]", "win_update_whitelist:": "[]", "win_update_category_names": "[ CriticalUpdates, DefinitionUpdates, SecurityUpdates, ServicePacks, UpdateRollups, Updates ]" }
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Send Windows Updates Report"
    org: Default
    inv: "Azure Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/windows-update-report.yml"
    creds:
      - "Windows Credential"
    ask_limit_on_launch: yes
    ask_variables_on_launch: no
    allow_simultaneous: no
    extra_vars: { "win_update_category_names": "[ CriticalUpdates, DefinitionUpdates, SecurityUpdates, ServicePacks, UpdateRollups, Updates ]", "send_email_sendgrid": "no", "sendgrid_from_email_address": "example@example.com", "sendgrid_to_email_addresses": "example@example.com", "send_email_smtp": "no", "send_slack_notification": "no", "save_report_locally": "no" }
    execution_environment: "Azure Execution Environment"
  - name: "[JT][WIN DEMO] Pull report from WSUS"
    org: Default
    inv: "Azure Inventory"
    project: "Windows Demo Environment Project"
    playbook: "playbooks/windows-report-wsus.yml"
    creds:
      - "Windows Credential"
    ask_limit_on_launch: yes
    ask_variables_on_launch: no
    allow_simultaneous: no
    execution_environment: "Azure Execution Environment"

scm_workflowjobtemplates:
  - name: "[WF][WIN DEMO] Create Windows Demo Infra - CLIENT + DC + WSUS"
    org: Default
    inv: ""
    steps:
       - identifier: Windows Project
         unified_job_template:
           organization:
             name: Default
           name: "Windows Demo Environment Project"
           type: project
         credentials: []
         related:
           success_nodes:
             - identifier: "Create Azure Network"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Create Azure Network
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Create Azure Network"
           type: job_template
         credentials: []
         extra_data: {
           private_network: "windows-network"
         }
         related:
           success_nodes:
             - identifier: "Deploy Azure Instance - win-dc-1"
             - identifier: "Deploy Azure Instance - win-wsus-1"
             - identifier: "Deploy Azure Instance - win-cli-1"
             - identifier: "Deploy Azure Instance - win-cli-2"
             - identifier: "Deploy Azure Instance - win-cli-3"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Deploy Azure Instance - win-dc-1
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Deploy Azure Instance"
           type: job_template
         credentials: []
         extra_data: {
           instance_name: "win-dc-1",
           dc_role: "domain",
           windows_so_version: "Windows Server 2022 Datacenter"
         }
         related:
           success_nodes:
             - identifier: "Azure Inventory"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Deploy Azure Instance - win-wsus-1
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Deploy Azure Instance"
           type: job_template
         credentials: []
         extra_data: {
           instance_name: "win-wsus-1",
           dc_role: "wsus",
           windows_so_version: "Windows Server 2022 Datacenter"
         }
         related:
           success_nodes:
             - identifier: "Azure Inventory"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Deploy Azure Instance - win-cli-1
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Deploy Azure Instance"
           type: job_template
         credentials: []
         extra_data: {
           instance_name: "win-cli-1",
           dc_role: "client",
           windows_so_version: "Windows Server 2022 Datacenter"
         }
         related:
           success_nodes:
             - identifier: "Azure Inventory"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Deploy Azure Instance - win-cli-2
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Deploy Azure Instance"
           type: job_template
         credentials: []
         extra_data: {
           instance_name: "win-cli-2",
           dc_role: "client",
           windows_so_version: "Windows Server 2019 Datacenter"
         }
         related:
           success_nodes:
             - identifier: "Azure Inventory"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Deploy Azure Instance - win-cli-3
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Deploy Azure Instance"
           type: job_template
         credentials: []
         extra_data: {
           instance_name: "win-cli-3",
           dc_role: "client",
           windows_so_version: "Windows Server 2016 Datacenter"
         }
         related:
           success_nodes:
             - identifier: "Azure Inventory"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Azure Inventory
         all_parents_must_converge: true
         unified_job_template:
           inventory:
             organization:
               name: Default
           name: "Azure Inventory Source"
           type: inventory_source
         credentials: []
         related:
           success_nodes:
             - identifier: "Deploy Windows Myrtille"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Deploy Windows Myrtille
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Deploy Windows Myrtille"
           type: job_template
         credentials: []
         limit: win-dc-1
         related:
           success_nodes:
             - identifier: "Deploy Windows Domain Controller"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Deploy Windows Domain Controller
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Deploy Windows Domain Controller"
           type: job_template
         credentials: []
         limit: win-dc-1
         related:
           success_nodes:
             - identifier: "Join Windows Client to a Domain"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Join Windows Client to a Domain
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Join Windows Client to a Domain"
           type: job_template
         credentials: []
         related:
           success_nodes:
             - identifier: "Deploy Windows Server Update Services"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Deploy Windows Server Update Services
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Deploy Windows Server Update Services"
           type: job_template
         credentials: []
         limit: win-wsus-1
         extra_data: {
           wsus_products: "Windows Server 2016, Windows Server 2019, Microsoft Server operating system-21H2, Microsoft Defender Antivirus"
         }
         related:
           success_nodes:
             - identifier: "Configure Windows Firewall"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Configure Windows Firewall
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Configure Windows Firewall"
           type: job_template
         credentials: []
         limit: win-wsus-1,win-cli-1,win-cli-2,win-cli-3
         related:
           success_nodes:
             - identifier: "Create WSUS Group Policy"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Create WSUS Group Policy
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Create WSUS Group Policy"
           type: job_template
         credentials: []
         limit: win-dc-1
         related:
           success_nodes:
             - identifier: "Join Windows Client to a WSUS"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Join Windows Client to a WSUS
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Join Windows Client to a WSUS"
           type: job_template
         credentials: []
         extra_data: {
           register_with_wsus: "yes"
         }
         related:
           success_nodes: []
           failure_nodes: []
           always_nodes: []
           credentials: []
  - name: "[WF][WIN DEMO] Create Windows Demo Infra - CLIENT + DC"
    org: Default
    inv: ""
    steps:
       - identifier: Windows Project
         unified_job_template:
           organization:
             name: Default
           name: "Windows Demo Environment Project"
           type: project
         credentials: []
         related:
           success_nodes:
             - identifier: "Create Azure Network"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Create Azure Network
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Create Azure Network"
           type: job_template
         credentials: []
         extra_data: {
           private_network: "windows-network"
         }
         related:
           success_nodes:
             - identifier: "Deploy Azure Instance - win-dc-1"
             - identifier: "Deploy Azure Instance - win-cli-1"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Deploy Azure Instance - win-dc-1
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Deploy Azure Instance"
           type: job_template
         credentials: []
         extra_data: {
           instance_name: "win-dc-1",
           dc_role: "domain",
           windows_so_version: "Windows Server 2022 Datacenter"
         }
         related:
           success_nodes:
             - identifier: "Azure Inventory"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Deploy Azure Instance - win-cli-1
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Deploy Azure Instance"
           type: job_template
         credentials: []
         extra_data: {
           instance_name: "win-cli-1",
           dc_role: "client",
           windows_so_version: "Windows Server 2022 Datacenter"
         }
         related:
           success_nodes:
             - identifier: "Azure Inventory"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Azure Inventory
         all_parents_must_converge: true
         unified_job_template:
           inventory:
             organization:
               name: Default
           name: "Azure Inventory Source"
           type: inventory_source
         credentials: []
         related:
           success_nodes:
             - identifier: "Deploy Windows Myrtille"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Deploy Windows Myrtille
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Deploy Windows Myrtille"
           type: job_template
         credentials: []
         limit: win-dc-1
         related:
           success_nodes:
             - identifier: "Deploy Windows Domain Controller"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Deploy Windows Domain Controller
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Deploy Windows Domain Controller"
           type: job_template
         credentials: []
         limit: win-dc-1
         related:
           success_nodes:
             - identifier: "Join Windows Client to a Domain"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Join Windows Client to a Domain
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Join Windows Client to a Domain"
           type: job_template
         credentials: []
         related:
           success_nodes: []
           failure_nodes: []
           always_nodes: []
           credentials: []
  - name: "[WF][WIN DEMO] Remove Windows Demo Infra - CLIENT + DC + WSUS"
    org: Default
    inv: ""
    steps:
       - identifier: Remove Azure Instance - win-dc-1
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Remove Azure Instance"
           type: job_template
         credentials: []
         extra_data: {
           instance_name: "win-dc-1"
         }   
         related:
           success_nodes:
             - identifier: "Remove Azure Network"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Remove Azure Instance - win-wsus-1
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Remove Azure Instance"
           type: job_template
         credentials: []
         extra_data: {
           instance_name: "win-wsus-1"
         }   
         related:
           success_nodes: 
             - identifier: "Remove Azure Network"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Remove Azure Instance - win-cli-1
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Remove Azure Instance"
           type: job_template
         credentials: []
         extra_data: {
           instance_name: "win-cli-1"
         }
         related:
           success_nodes:
             - identifier: "Remove Azure Network"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Remove Azure Instance - win-cli-2
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Remove Azure Instance"
           type: job_template
         credentials: []
         extra_data: {
           instance_name: "win-cli-2"
         }
         related:
           success_nodes:
             - identifier: "Remove Azure Network"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Remove Azure Instance - win-cli-3
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Remove Azure Instance"
           type: job_template
         credentials: []
         extra_data: {
           instance_name: "win-cli-3"
         }
         related:
           success_nodes:
             - identifier: "Remove Azure Network"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Remove Azure Network
         all_parents_must_converge: true
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Remove Azure Network"
           type: job_template
         credentials: []
         extra_data: {
           private_network: "windows-network"
         }
         related:
           success_nodes: []
           failure_nodes: []
           always_nodes: []
           credentials: []
  - name: "[WF][WIN DEMO] Remove Windows Demo Infra - CLIENT + DC"
    org: Default
    inv: ""
    steps:
       - identifier: Remove Azure Instance - win-dc-1
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Remove Azure Instance"
           type: job_template
         credentials: []
         extra_data: {
           instance_name: "win-dc-1"
         }
         related:
           success_nodes:
             - identifier: "Remove Azure Network"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Remove Azure Instance - win-cli-1
         unified_job_template:
           organization:    
             name: Default
           name: "[JT][WIN DEMO] Remove Azure Instance"
           type: job_template
         credentials: []
         extra_data: {
           instance_name: "win-cli-1"
         }
         related:
           success_nodes:
             - identifier: "Remove Azure Network"
           failure_nodes: []
           always_nodes: []
           credentials: []
       - identifier: Remove Azure Network
         all_parents_must_converge: true
         unified_job_template:
           organization:
             name: Default
           name: "[JT][WIN DEMO] Remove Azure Network"
           type: job_template
         credentials: []
         extra_data: {
           private_network: "windows-network"
         }
         related:
           success_nodes: []
           failure_nodes: []
           always_nodes: []
           credentials: []
